FROM ruby:3.2-alpine

RUN apk update && apk add build-base
ENV BUNDLER_VERSION=2.4.13
RUN gem install bundler:$BUNDLER_VERSION

COPY Gemfile* .

RUN bundler install